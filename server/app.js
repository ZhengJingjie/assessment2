/**
 * Created by Jingjie on 8/11/16.
 */

var express = require("express");
var Sequelize = require("sequelize");

var app = express();

const PORT = 3000;

var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = "Mysqlpassword22";

var conn = new Sequelize('groceries', MYSQL_USERNAME, MYSQL_PASSWORD, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

var groceryList = require('./model/grocery_list')(conn, Sequelize);

app.get("/api/groceries", function (req, res) {
    console.log("GET /api/groceries");
    grocery_list
        .findAll({
            order: [["name", "Desc"]],
            attributes: ["upc12", "brand", "name"],
            limit: 20
        })
        .then(function (groceries) {
            console.log("GET /api/groceries successful");
            res.json(groceries);
        })
        .catch(function (err) {
            console.log("err " + err);
            res
                .status(500)
                .json({error: true})
        });
});

app.use(express.static(__dirname + "/public"));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

app.listen(PORT, function () {
    console.info("App Server started on port", PORT);
});

