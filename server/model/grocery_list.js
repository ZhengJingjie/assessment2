/**
 * Created by Jingjie on 8/11/16.
 */

module.exports = function (conn, Sequelize) {
    var groceryList = conn.define("grocery_list", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        upc12: {
            type: Sequelize.INTEGER
        },
        brand: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    return groceryList;
};